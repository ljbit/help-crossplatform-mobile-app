// Initialize your app
msgcount=1;
Topic=1;  // topic number 1 by default
var myApp = new Framework7();
var training_audio=null;

var mySwiper = myApp.swiper('.swiper-container', {
    pagination:'.swiper-pagination'
  });

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

// Callbacks to run specific code for specific pages, for example for About page:
myApp.onPageInit('about', function (page) {
    // run createContentPage func after link was clicked
    $$('.create-page').on('click', function () {
        createContentPage();
    });
});

// Generate dynamic page
var dynamicPageIndex = 0;
function createContentPage() {
	mainView.router.loadContent(
        '<!-- Top Navbar-->' +
        '<div class="navbar">' +
        '  <div class="navbar-inner">' +
        '    <div class="left"><a href="#" class="back link"><i class="icon icon-back"></i><span>Back</span></a></div>' +
        '    <div class="center sliding">Dynamic Page ' + (++dynamicPageIndex) + '</div>' +
        '  </div>' +
        '</div>' +
        '<div class="pages">' +
        '  <!-- Page, data-page contains page name-->' +
        '  <div data-page="dynamic-pages" class="page">' +
        '    <!-- Scrollable page content-->' +
        '    <div class="page-content">' +
        '      <div class="content-block">' +
        '        <div class="content-block-inner">' +
        '          <p>Here is a dynamic page created on ' + new Date() + ' !</p>' +
        '          <p>Go <a href="#" class="back">back</a> or go to <a href="services.html">Services</a>.</p>' +
        '        </div>' +
        '      </div>' +
        '    </div>' +
        '  </div>' +
        '</div>'
    );
	return;
}

function loadpage(PageId)
{
    if (PageId==4)
    {
        myApp.closePanel();
        var url="http://www.leaphealthmobile.com/talk-to-us/";
        var ref = cordova.InAppBrowser.open(url, '_system', 'location=yes');
        
    }
    
    myApp.closePanel();
    
}

// page init event
$$(document).on('pageInit', function (e) {
   var page = e.detail.page;
    switchOffAudio(); //switch off audio on every page change
   if (page.name=="topic_message")
   {
       if (Topic==1){
           document.getElementById("topic").innerHTML = "Topic " +Topic;
           document.getElementById("topic_desc").innerHTML = "Family Planning";
           msgcount=1;
           showSMS();
       }
          
       else
       {
           document.getElementById("topic").innerHTML = "Topic " +Topic;
           document.getElementById("topic_desc").innerHTML = "Ebola";
           msgcount=1;
           showSMS();
       }
           
   }
});

 

function setTopicNumber(topicNumber)
{
    Topic=topicNumber;
}

function switchOffAudio()
{
   
    if (training_audio==null || training_audio.autoplayPaused)
    {}
    else
        training_audio.pause();
    var vcontrol = document.getElementById("videocontrol"); 
    if (vcontrol==null )
        return;
    
    vcontrol.pause();
}

function showSMS(){
    //alert("text");
    var sms="";
   
    document.getElementById("smsbutton").value = "Show next";
    switchOffAudio();
    
    if (msgcount==1 && Topic==1)
        sms="1/3 hello dear learner, welcome to the community health volunteer training. In this topic you will learn about, Family Planning.";
    else if (msgcount==1 && Topic==2)
        sms="1/4 hello dear learner and welcome to this sub-topic. In this sub-topic you will learn about Ebola.";
    else if (msgcount==2 && Topic==1)
        sms="2/3 During the week you will receive SMS messages and listen to recorded phone stories and lectures all related to Drug and Family Planning";
    else if (msgcount==2 && Topic==2)
        sms="2/4 During the week you will receive SMS messages and listen to recorded phone stories and lectures all related to Drug and Family Planning";
    else if (msgcount==3  && Topic==1)
        sms="3/3 Please follow the instructions to go through all the activities. You can contact your CHEW for technical help on using the mobile phone";
    else if (msgcount==3 && Topic==2)
        sms="3/4 Please follow the instructions to go through all the activities. You can contact your CHEW for technical help on using the mobile phone";
    else if (msgcount==4 && Topic==1){
        sms="To start the fist activity please dial, 0800 722 689 ";
        document.getElementById("smsbutton").value = "Play audio";
    }
     else if (msgcount==4 && Topic==2){
        sms="4/4 To start the fist activity please dial, 0800 722 689 ";
        document.getElementById("smsbutton").value = "Play audio";
    }
    else if (msgcount==5 && Topic==1){
        document.getElementById("smsbutton").value = "Play next audio";
        training_audio = new Audio('sounds/STE13-1S1A2F1.wav');
        training_audio.play();
    }
    else if (msgcount==5 && Topic==2){
        document.getElementById("smsbutton").value = "Play next audio";
        training_audio = new Audio('sounds/STE30-2S1A2F1.wav');
        training_audio.play();
    }
    else if (msgcount==6 && Topic==1){
        //sound.pause();
        // sound.currentTime = 0;
        training_audio = new Audio('sounds/STE13-2S1A2F1.wav');
        training_audio.play();
        document.getElementById("smsbutton").value = "Play next audio";
    }
    else if (msgcount==6 && Topic==2){
        //sound.pause();
        // sound.currentTime = 0;
        training_audio = new Audio('sounds/STE30-2S1A2F2.wav');
        training_audio.play();
        document.getElementById("smsbutton").value = "Start again";
        msgcount=0;
    }
    else if (msgcount==7 && Topic==1){
        //sound.pause();
         //sound.currentTime = 0;
        training_audio = new Audio('sounds/STE13-2S2A1F1.wav');
        training_audio.play();
        document.getElementById("smsbutton").value = "Start again";
        msgcount=0;
            }
    else  {
        //sound.pause();
        // sound.currentTime = 0;
        msgcount=1;
        sms="msg1";
        
    }
    
    if (msgcount==1 || msgcount==2 || msgcount==3|| msgcount==4)
        document.getElementById("smsbox").innerHTML = sms;
    
     msgcount+=1;
    return;
}