cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.shoety.cordova.plugin.inappbrowserxwalk/www/inappbrowserxwalk.js",
        "id": "com.shoety.cordova.plugin.inappbrowserxwalk.inAppBrowserXwalk",
        "clobbers": [
            "inAppBrowserXwalk"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.2.2",
    "com.shoety.cordova.plugin.inappbrowserxwalk": "0.3.3",
    "cordova-plugin-crosswalk-webview": "2.0.0"
};
// BOTTOM OF METADATA
});